<?php

namespace App\Http\Controllers\Website\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterUserRequest;
use App\Models\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index(){
        return view('website.pages.register');
    }
    public function register(RegisterUserRequest $request){
        $request->validated();
        try {
            User::create($request);
            session()->flash('success', 'Register Success');
            return redirect()->back();
        } catch (\Exception $e){
            session()->flash('fail', 'Register fail');
        }

    }
}
