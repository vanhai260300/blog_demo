<?php

namespace App\Http\Controllers\Website\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    //
    public function index(){
        return view('website.pages.login');
    }
    public function login(Request $request){
        $credentials = $request->validate([
            'email' => ['required','email'],
            'password' => ['required']
        ], [
            'email.require' => 'Email Require',
            'email.email' => 'Is Email',
            'password' => 'Password require'
        ]);
//        ['email' => $request->email,'password' => $request->password]
        if (Auth::attempt($credentials))
        {
            session()->flash('success', 'Login Success');
            return redirect()->intended('')->with( ['succses' => 1] );
        } else {
            return back()->withErrors(
                ['message' => 'Login information is incorrect, please check again']
            )->withInput();
        }
    }
    public function logout(){
        session()->flush();
        Auth::logout();
        return redirect()->route("website.index");
    }
}
