<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Entry;
use App\Models\Follower;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function index(){
        if(Auth::check() == true)
        {
            $users = User::with('followed')->with('followers')->where('id', Auth::id())->first();
            $data['users'] = $users;
        }
        $entries = Entry::with('user')->with('comments')->orderBy('post_date', 'DESC')->get();
        $data['entries'] = $entries;
//        dd($entries);

        return view('website.pages.home', $data);
    }
    public function comment(Request $request)
    {
//        $data = $request->all();
//        $data['user_id'] = Auth::id();
//        Comment::create($data);
        $comment = new Comment();
        $comment -> comment = $request -> comment;
        $comment -> user_id = Auth::id();
        $comment -> entry_id = $request -> entry_id;
        $comment -> save();
        return $request->comment;

    }
    public function changeFollow(Request $request)
    {
        $res = 0;
        $user_id = $request->user_id;
        $auth_id = Auth::id();
        if($request -> isfollow == 1){
            $follow = Follower::where([['user_id', $user_id],['follower_id', $auth_id]])->first();
            $res = $follow->id;
            $follow -> delete();

        }
        if($request -> isfollow == 2){
            $follow = new Follower();
            $follow -> user_id = $user_id;
            $follow -> follower_id = $auth_id;
            $follow -> save();
            $res = 2;
        }
        return $res;
    }

}
