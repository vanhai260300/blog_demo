<?php

namespace App\Http\Controllers\Website;
use App\Models\Entry;
use File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class EntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entries = Entry::with('comments')->paginate(10);
        $data['entries'] = $entries;
//        dd($entries);
        return view('website.pages.entry.entry', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('website.pages.entry.addentry');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        try{
            DB::beginTransaction();
            $entry =  new Entry();
            $entry->user_id = Auth::id();
            $entry->title = $request->title;
            $entry->content = $request->contentt;
            $entry->post_date = now();
            $image=$request->image;
            $name_image = time().$entry->id.'.'.$request->image->extension();
            Storage::disk('public')->put($name_image, File::get($image));
            $entry->image = $name_image;
            $entry -> save();
            DB::commit();
            return redirect()->route('website.index');
        } catch (\Exception $e){
            DB::rollBack();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('website.pages.entry.editentry');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $entry = Entry::find($id);
        try {
            DB::beginTransaction();
            $entry->delete();
            $entry->comments()->detach($entry->comments);
            DB::commit();
            return redirect()->back();
        } catch (Exception $e){
            DB::rollBack();
            return redirect()->back();
        }
    }
}
