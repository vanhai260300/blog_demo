<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Please enter Name',
            'email.required' => 'Please enter Email',
            'email.email' => 'Please enter Email',
            'email.unique' => 'Email already exists',
            'password' => 'Please enter password'
        ];
    }
}
