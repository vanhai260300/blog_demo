<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    use HasFactory;
//    public function comments(){
//        return $this->hasMany(Comment::class, 'entry_id', 'id');
//    }
    public function comments(){
        return $this->belongsToMany(User::class,Comment::class, 'entry_id', 'user_id')->withPivot(['comment']);
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
