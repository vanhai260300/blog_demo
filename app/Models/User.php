<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use PHPUnit\Exception;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
//    public function followers(){
//        return $this->hasMany(Follower::class, 'user_id', 'id');
//    }
    public function followers(){
        return $this->belongsToMany(User::class, Follower::class, 'user_id', 'follower_id');
    }
    public function followed(){
        return $this->belongsToMany(User::class, Follower::class, 'follower_id', 'user_id');
    }
    public function comments(){
        return $this->belongsToMany(Entry::class,Comment::class, 'user_id', 'entry_id')->withPivot(['comment']);
    }
    public function entries(){
        return $this->hasMany(Entry::class);
    }
    public static function create($request)
    {
        try {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->birthdate = now();
            $user->is_role = 2;
            return $user->save();
        } catch (\Exception $e){
            return false;
        }

    }
    public static function updateuser($request, $id){
        try {
            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->birthdate = $request->birthdate;
            return $user->save();
        } catch (\Exception $e){
            return false;
        }

    }
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
