@extends('website.layouts.master')
@section('content')
    <!---start-wrap---->
    <div class="row">
        <div class="col-sm-9 pe-0">
            <div class="content ">

                <div class="contact">
                    <div class="contact-form">
                        <h3>Sign Up</h3>
                        <form action="{{route('website.signup')}}" method="POST" class="px-3">
                            @csrf
                            <label for="name">Name</label>
                            <input name="name" placeholder="Enter Name" class="form-control my-2" value="{{ old('name') }}">
                            <label for="email">Email</label>
                            <input type="email" name="email" placeholder="Enter Email" class="form-control my-2" value="{{ old('email') }}">
                            <label for="" >Password</label>
                            <input type="password" name="password" placeholder="***********" class="form-control my-2" value="{{ old('password') }}">
                            <label for="password_confirmation">Password Confirmed</label>
                            <input type="password" id="password_confirmation" name="password_confirmation" placeholder="***********" class="form-control my-2">
                            <input type="submit" value="Sign Up">
                        </form>
                    </div>
                    <div class="contact-address">
                        <h3>Contact-Address</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        <ul>
                            <li><span><img src="{{asset('libs/images/mobile.png')}}" title="mobileno" />+404(0) 1234 5789</span></li>
                            <li><a href="mailto:company@gmail.com"><img src="{{asset('libs/images/sms.png')}}" title="message" />hello@demolinks.com</a></li>
                            <li><a href="#"><img src="{{asset('libs/images/skep.png')}}" title="skeep" />step.man04</a></li>
                            <li><a href="#"><img src="{{asset('libs/images/twit.png')}}" title="twitter" />twitter.com/jan</a></li>
                        </ul>
                    </div>
                    <div class="clear"> </div>
                </div>
                <div class="clear"> </div>
                <div class="footer">
                    <p>&#169 2013 Feedlive . All Rights Reserved | Design By <a href="http://w3layouts.com/">W3Layouts</a>
                    </p>
                </div>
                <div class="clear"> </div>

            </div>
        </div>
        <div class="col-sm-3">
            <div class="right-sidebar ">
                <div class="search-bar">
                    <form>
                        <input type="text" value="Search" onfocus="this.value = '';"
                               onblur="if (this.value == '') {this.value = 'Search';}" />
                        <input type="submit" value="" />
                    </form>
                </div>
                <div class="clear"> </div>
                <div class="featured-Videos">
                    <h3>Featured Videos</h3>
                    <a href="#"><img src="{{asset('libs/images/videos.jpg')}}" title="videos" /></a>
                </div>
                <div class="popular-post">
                    <h3>popular-posts</h3>
                    <div class="post-grid">
                        <img src="{{asset('libs/images/videos.jpg')}}" title="post1">
                        <p>Lorem ipsum dolor sit ametconsectetur dolor,<a href="#">...</a></p>
                        <div class="clear"> </div>
                    </div>
                    <div class="post-grid">
                        <img src="{{asset('libs/images/videos.jpg')}}" title="post1">
                        <p>Lorem ipsum dolor sit ametconsectetur dolor,<a href="#">...</p>
                        <div class="clear"> </div>
                    </div>
                    <div class="view-all">
                        <a href="#">ViewAll</a>
                    </div>
                </div>
                <div class="clear"> </div>
            </div>
        </div>

    </div>
    {{--    modal create post--}}
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center">
                        <h2 class="fs-4 fw-bold">Create post</h2>
                    </div>

                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="d-flex">
                            <div class="image">
                                <img class="rounded-circle" src="{{asset('libs/images/videos.jpg')}}" alt="" width="40px" height="40px">
                            </div>
                            <p class="align-self-center ps-3">Nguyen Van Hai</p>
                        </div>
                        <div class="mb-3">
                            <label for="message-text" class="col-form-label">Content:</label>
                            <textarea class="form-control border-0" id="message-text" rows="5"  placeholder="What are you thinking?"></textarea>
                            <input type="file" name="" id="">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Post</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!---end-sidebar---->
    <!----start-content----->
    @push('custom-scripts')
        <script type="text/javascript">
            @if ($errors -> any())
                toastr.options.timeOut = 3000; // How long the toast will display without user interaction
                toastr.options.extendedTimeOut = 3000; // How long the toast will display after a user hovers over it
                toastr.error("Register is fail","Fail");
            @endif
        </script>
    @endpush
@endsection


