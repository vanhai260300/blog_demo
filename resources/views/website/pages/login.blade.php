@extends('website.layouts.master')
@section('content')
    <!---start-wrap---->
    <div class="row">
        <div class="col-sm-9 pe-0">
            <div class="content ">

                <div class="contact">
                    <div class="contact-form">
                        <h3>Login</h3>
                        <form action="{{ route('website.login') }}" method="POST" class="px-3">
                            @csrf
{{--                            {{ dd($errors->all()) }}--}}
{{--                            <div class="error">--}}
{{--                                <ul>--}}
{{--                                    @if ($errors -> any())--}}
{{--                                        --}}{{--                    {{ dd($errors) }}--}}
{{--                                        @foreach($errors->all() as $key => $value)--}}
{{--                                            <li class="text-danger">{{ $value }}</li>--}}
{{--                                        @endforeach--}}
{{--                                    @endif--}}
{{--                                </ul>--}}
{{--                            </div>--}}
                            <label for="email">Email</label>
                            <input type="email" id="email" name="email" placeholder="Enter Email" class="form-control my-2" value="{{ old('email') }}">
                            <div>
                                @error('email')
                                <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>

                            <label for="password">Password</label>
                            <input type="password" id="password" name="password" placeholder="***********" class="form-control my-2">
                            <div>
                                @error('password')
                                <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <input type="submit" value="Login">
                        </form>
                    </div>
                    <div class="contact-address">
                        <h3>Contact-Address</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        <ul>
                            <li><span><img src="{{asset('libs/images/mobile.png')}}" title="mobileno" />+ 0376445796</span></li>
                            <li><a href="mailto:company@gmail.com"><img src="{{asset('libs/images/sms.png')}}" title="message" />vanhai@links.com</a></li>
                            <li><a href="#"><img src="{{asset('libs/images/skep.png')}}" title="skeep" />step.man04</a></li>
                            <li><a href="#"><img src="{{asset('libs/images/twit.png')}}" title="twitter" />twitter.com/jan</a></li>
                        </ul>
                    </div>
                    <div class="clear"> </div>
                </div>
                <div class="clear"> </div>
                <div class="footer">
                    <p>&#169 2013 Feedlive . All Rights Reserved | Design By <a href="http://w3layouts.com/">W3Layouts</a>
                    </p>
                </div>
                <div class="clear"> </div>

            </div>
        </div>
        <div class="col-sm-3">
            <div class="right-sidebar ">
                <div class="search-bar">
                    <form>
                        <input type="text" value="Search" onfocus="this.value = '';"
                               onblur="if (this.value == '') {this.value = 'Search';}" />
                        <input type="submit" value="" />
                    </form>
                </div>
                <div class="clear"> </div>
                <div class="featured-Videos">
                    <h3>Featured Videos</h3>
                    <a href="#"><img src="{{asset('libs/images/videos.jpg')}}" title="videos" /></a>
                </div>
                <div class="popular-post">
                    <h3>popular-posts</h3>
                    <div class="post-grid">
                        <img src="{{asset('libs/images/videos.jpg')}}" title="post1">
                        <p>Lorem ipsum dolor sit ametconsectetur dolor,<a href="#">...</a></p>
                        <div class="clear"> </div>
                    </div>
                    <div class="post-grid">
                        <img src="{{asset('libs/images/videos.jpg')}}" title="post1">
                        <p>Lorem ipsum dolor sit ametconsectetur dolor,<a href="#">...</p>
                        <div class="clear"> </div>
                    </div>
                    <div class="view-all">
                        <a href="#">ViewAll</a>
                    </div>
                </div>
                <div class="clear"> </div>
            </div>
        </div>

    </div>

    <!---end-sidebar---->
    <!----start-content----->
    @push('custom-scripts')
        <script type="text/javascript">
            @if ($errors -> any())
                toastr.options.timeOut = 3000; // How long the toast will display without user interaction
                toastr.options.extendedTimeOut = 3000; // How long the toast will display after a user hovers over it
                toastr.error("Login is fail","Fail");
            @endif
        </script>
    @endpush
@endsection


