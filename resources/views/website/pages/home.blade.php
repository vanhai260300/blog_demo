@extends('website.layouts.master')
@section('content')
<!---start-wrap---->
    <div class="row">
        <div class="col-sm-9 pe-0">
            <div class="content my-2 d-flex">
                @if(Auth::check())
                <div class="image">
                    <img class="rounded-circle" src="{{ Auth::user()->image }}" alt="" width="40px" height="40px">
                </div>
                <input type="text" class="form-control rounded-pill border-bottom ms-2" placeholder="What are you thinking?" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@fat">
                @endif
            </div>
            <div class="content ">

                <div class="grids">
                    @foreach ($entries as $key => $entry)

                    <div class="grid box py-2">
                        <div class="grid-header">


                            <h3>{{$entry->title}} </h3>
                            <ul>
                                <li><span>Post By <a href="#" class="fs-6">{{ $entry->user->name }}</a> on {{ $entry->post_date }}</span></li>
                                <li><a href="#">{{ count($entry->comments) }} comments</a></li>
                                @if(Auth::check() == true)
                                    @if(Auth::user()->id == $entry->user_id)
                                    @else
                                        @php
                                            $result = false
                                        @endphp
                                        @foreach($users->followed as $key => $value )
                                            @if($value -> id == $entry->user_id)
                                                @php
                                                    $result = true
                                                @endphp
                                            @endif
                                        @endforeach
                                        @if($result === true)
                                                <li><buttom onclick="changeFollow({{ $entry->user->id }})" role="button" class="p-1 rounded btn-light text-dark fs-6 followed_{{ $entry->user->id }}" >Followed</buttom></li>
                                                <li><buttom onclick="changeFollowed({{ $entry->user->id }})" role="button"  class="p-1 rounded btn-primary text-white fs-6 hidden follow_{{ $entry->user->id }}">Follow</buttom></li>
                                        @else
                                                <li><buttom onclick="changeFollowed({{ $entry->user->id }})" role="button" class="p-1 rounded btn-primary text-white fs-6 follow_{{ $entry->user->id }}">Follow</buttom></li>
                                                <li><buttom onclick="changeFollow({{ $entry->user->id }})" role="button" class="p-1 rounded btn-light text-dark fs-6 hidden followed_{{ $entry->user->id }}">Followed</buttom></li>
                                        @endif
                                    @endif
                                @endif
                            </ul>

                        </div>
                        <div class="grid-img-content">
                            <a href="singlepage.html">
{{--                                <img src="{{asset('libs/images/1.jpg')}}" />--}}
                                <img src="{{ Storage::disk('public')->exists($entry->image) == true ? Storage::url($entry->image) : $entry->image }}"  width="140px" height="140px"/>
                            </a>
                            <p>{{ $entry->content }}<a href="#">...</a></p>
                            <div class="clear"> </div>
                        </div>
                        <div class="comments">
                            <div>
                                <ul>
                                    <li><a href="#"><img src="{{asset('libs/images/views.png')}}" title="view" /></a></li>
                                    <li><a href="#"><img src="{{asset('libs/images/likes.png')}}" title="likes" /></a></li>
                                    <li><a href="#"><img src="{{asset('libs/images/link.png')}}" title="link" /></a></li>
                                    <li><a class="readmore" href="#">ReadMore</a></li>
                                </ul>
                            </div>
                            <div>
                                <div class="comment-content" id="comment-content_{{ $entry->id }}">

                                        @foreach( $entry->comments as $key => $comment )
                                            <div class="d-flex py-2">
                                                <div class="flex-shrink-0"><a href=""><img class="rounded-circle" src="{{ Storage::disk('public')->exists($comment->image) == true ? Storage::url($comment->image) : $comment->image }}" width="40px" height="40px" alt="..."></a></div>
                                                <div class="ms-3">
                                                    <div class="fw-bold d-block"><a href="" class="text-dark">{{$comment->name}}</a></div>
                                                    <p class="comment_item">{{ ($comment->pivot->comment) }}
                                                    </p>
                                                </div>
                                            </div>
                                        @endforeach

                                </div>
                                @if(Auth::check() == true)
                                    @php
                                        $check = false;
                                    @endphp
                                    @foreach($users->followed as $key => $value )
                                        @if($value -> id == $entry->user_id || Auth::user()->id == $entry->user_id)
                                            @php
                                                $check = true;
                                            @endphp
                                        @endif
                                    @endforeach
                                    @if($check == true)
                                        <ul class="d-flex">
                                            <div class="image">
                                                <a href="#"><img class="rounded-circle" src="{{ Auth::user()->image }}" alt="" width="40px" height="40px"></a>

                                            </div>
                                            <form id="form-comment_{{ $entry->id }}" onsubmit="commentonpost(event, {{ $entry->id }})" class="form-control border-0 p-0">
                                                <input type="text" name="comment" class="form-control rounded-pill border-bottom ms-2" placeholder="Comment">
                                            </form>
                                        </ul>
                                    @endif
                                @endif
                            </div>

                        </div>

                    </div>
                    <div class="clear"> </div>
                    @endforeach
                </div>
                <div class="clear"> </div>
                <div class="content-pagenation">
                    <li><a href="#">First</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">Last</a></li>
                    <div class="clear"> </div>
                </div>
                <div class="clear"> </div>
                <div class="footer">
                    <p>&#169 2013 Feedlive . All Rights Reserved | Design By <a href="http://w3layouts.com/">W3Layouts</a>
                    </p>
                </div>
                <div class="clear"> </div>

            </div>
        </div>
        <div class="col-sm-3">
            <div class="right-sidebar ">
                <div class="search-bar">
                    <form>
                        <input type="text" value="Search" onfocus="this.value = '';"
                               onblur="if (this.value == '') {this.value = 'Search';}" />
                        <input type="submit" value="" />
                    </form>
                </div>
                <div class="clear"> </div>
                <div class="featured-Videos">
                    <h3>Featured Videos</h3>
                    <a href="#"><img src="{{asset('libs/images/videos.jpg')}}" title="videos" /></a>
                </div>
                <div class="popular-post">
                    <h3>popular-posts</h3>
                    <div class="post-grid">
                        <a href="#"><img src="{{asset('libs/images/videos.jpg')}}" title="post1"></a>
                        <p>Lorem ipsum dolor sit ametconsectetur dolor,<a href="#">...</a></p>
                        <div class="clear"> </div>
                    </div>
                    <div class="post-grid">
                        <a href="#"><img src="{{asset('libs/images/videos.jpg')}}" title="post1"></a>

                        <p>Lorem ipsum dolor sit ametconsectetur dolor,<a href="#">...</p>
                        <div class="clear"> </div>
                    </div>
                    <div class="view-all">
                        <a href="#">ViewAll</a>
                    </div>
                </div>
                <div class="clear"> </div>
                @if(Auth::check() == true)
                <div class="follow">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Followed</button>
                            <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Followers</button>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <ul>
                                @foreach($users->followed as $key => $value )
                                <li class="p-2">
                                    <a href="" class="d-flex align-self-center">
                                        <div class="d-flex">
                                            <div class="image">
                                                <a href="#"><img class="rounded-circle" src="{{ $value->image }}" alt="" width="40px" height="40px"></a>

                                            </div>
                                            <p class="align-self-center ps-3">{{ $value->name }}</p>
                                        </div>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <ul>
                                @foreach($users->followers as $key => $value )
                                    <li class="p-2">
                                        <a href="">
                                            <div class="d-flex">
                                                <div class="image">
                                                    <a href="#"><img class="rounded-circle" src="{{ $value->image }}" alt="" width="40px" height="40px"></a>

                                                </div>
                                                <p class="align-self-center ps-3">{{ $value->name }}</p>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                    </div>
                </div>
                    @endif
            </div>
        </div>

    </div>
@if(Auth::check() == true)
{{--    modal create post--}}
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="text-center">
                        <h2 class="fs-4 fw-bold">Create post</h2>
                    </div>

                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('entry.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="d-flex">
                            <div class="image">
                                <img class="rounded-circle" src="{{ Auth::user()->image }}" alt="" width="40px" height="40px">
                            </div>
                            <p class="align-self-center ps-3">{{ Auth::user()->name }}</p>
                        </div>
                        <div>
                            <label for="title" class="col-form-label">Title:</label>
                            <input type="text" class="form-control" name="title" id="title" placeholder="Title of post">
                        </div>
                        <div class="mb-3">
                            <label for="contentt" class="col-form-label">Content:</label>
                            <textarea class="form-control border-0" name="contentt" id="contentt" rows="5"  placeholder="What are you thinking?"></textarea>
                            <input type="file" name="image" id="image">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary" value="Post">
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endif
    <!---end-sidebar---->
<!----start-content----->
@push('custom-scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

        });
        function commentonpost(event, idpost){
            var comment = $('#form-comment_'+ idpost +' input[name="comment"]').val();
            let comments = $('#comment-content_'+idpost);
            @if(Auth::check() == true)
            $.ajax({
                url:'{{ route('website.comment') }}',
                method: 'POST',
                data:{
                    comment : comment,
                    entry_id : idpost,
                    _token: '{{csrf_token()}}'
                },
                success: function (responsive){
                    console.log(responsive);
                }
            })
            comments.append(`<div class="d-flex py-2">
                                            <div class="flex-shrink-0"><a href=""><img class="rounded-circle" src="{{ Auth::user()->image }}" width="40px" height="40px" alt="..."></a></div>
                                            <div class="ms-3">
                                                <div class="fw-bold d-block"><a href="" class="text-dark">{{ Auth::user()->name }}</a></div>
                                                <p class="comment_item">${comment}
                                                </p>
                                            </div>
                                        </div>`);
            @endif
            $('#form-comment_'+ idpost +' input[name="comment"]').val('');
            event.preventDefault();
        }
        function changeFollow(iduser){
            const follow = 1;
            $(`.followed_${iduser}`).hide();
            $(`.follow_${iduser}`).show();
            $.ajax({
                url: '{{ route('website.changefollow') }}',
                method: 'POST',
                data: {
                    user_id: iduser,
                    isfollow: follow,
                    _token: '{{csrf_token()}}'
                },
                success: function (responsive){
                    console.log(responsive);
                }
            });
        }
        function changeFollowed(iduser){
            const followed = 2;
            $(`.follow_${iduser}`).hide();
            $(`.followed_${iduser}`).show();
            $.ajax({
                url: '{{ route('website.changefollow') }}',
                method: 'POST',
                data: {
                    user_id: iduser,
                    isfollow: followed,
                    _token: '{{csrf_token()}}'
                },
                success: function (responsive){
                    console.log(responsive);
                }
            });
        }
    </script>
@endpush
@endsection


