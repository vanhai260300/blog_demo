@extends('website.layouts.master')
@section('content')
    <div class="bg-light p-3">
        <h1>Manage Entry</h1>
        <a href="{{ route('entry.create') }}" class="btn btn-primary">Create Entry</a>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Content</th>
                <th>Posst date</th>
                <th>Posted by</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($entries as $key => $entry)
                <tr>
                    <td>{{ $entry->id }}</td>
                    <td>{{ $entry->title }}</td>
                    <td class="col-sm-x-2">{{ $entry->content }}</td>
                    <td>{{ $entry->post_date }}</td>
                    <td>{{ $entry->user->name }}</td>
                    <td>
                        <a href="{{ route('entry.edit',$entry->id) }}" class="">Edit</a>
                        <a href="{{ route('entry.delete',$entry->id) }}" class="text-danger" onclick="return confirm('Are you sure?')">Delete</a>
                        {{--                    <form id="delete-form-{{$user->id}}"--}}
                        {{--                          + action="{{route('users.destroy', $user->id)}}"--}}
                        {{--                          method="post">--}}
                        {{--                        @csrf @method('DELETE')--}}
                        {{--                    </form>--}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="number-pages">
            {{ $entries->links() }}
        </div>
    </div>

@endsection
