@extends('website.layouts.master')
@section('content')
    <form action="{{ route('entry.store') }}" method="post" id="new_form" class="bg-light p-3 border border-info p-3 rounded-3" enctype="multipart/form-data">
        {{ csrf_field() }}
        <h2 class="text-center">Create Posst</h2>
            @csrf
            <div class="d-flex">
                <div class="image">
                    <a href="#"><img class="rounded-circle" src="{{ Auth::user()->image }}" alt="" width="40px" height="40px"></a>

                </div>
                <p class="align-self-center ps-3">{{ Auth::user()->name }}</p>
            </div>
            <div>
                <label for="title" class="col-form-label">Title:</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="Title of post">
            </div>
            <div class="mb-3">
                <label for="contentt" class="col-form-label">Content:</label>
                <textarea class="form-control border-0" name="contentt" id="contentt" rows="5"  placeholder="What are you thinking?"></textarea>
                <input type="file" name="image" id="image">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Post">
            </div>

    </form>
@endsection
