<!doctype html>
<html lang="en">
<head>
    <title>Home Blog</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <meta name="keywords" content="FeedLive iphone web template" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link href='//fonts.googleapis.com/css?family=Merriweather+Sans' rel='stylesheet' type='text/css'>
    <link href="{{asset('libs/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
<div class="container-fluid"></div>
    <div class="wrap row">
        <nav class="col-sm-2">
            <div class="left-sidebar">
                <div class="logo">
                    <a href="index.html"><img src="{{asset('libs/images/logo.png')}}" title="logo" /></a>
                    <h1>Feedlive Blogger site</h1>
                </div>
                <div class="top-nav">
                    <ul>
                        <li><a href="{{ route('website.index') }}">Home</a></li>
                        @if(Auth::check() == true)
                            <li><a href="#">Profile</a></li>
                            <li><a href="{{ route('website.logout') }}">Logout</a></li>
                            @if(Auth::user()->is_role == 1)
                                <li><a href="{{ route('user.index') }}">Manege User</a></li>
                                <li><a href="{{ route('entry.index') }}">Manage Entry</a></li>
                            @endif
                        @else
                            <li><a href="{{ route('website.login.index') }}">Login</a></li>
                            <li><a href="{{ route('website.signup') }}">Sing up</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        <!---start-sidebar---->
        <main class="col-sm-10">
            @yield('content')
        </main>
    </div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script type="text/javascript">
        //errors
        @if(session('success'))
        toastr.success("{{ session('success') }}", "Notification");
        @endif
        @if(session('fail'))
        toastr.error("{{ session('fail') }}", "Notification");
        @endif
    </script>
    @stack('custom-scripts')
</body>
</html>
