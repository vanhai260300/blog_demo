@extends('welcome')
@section('content')
    <form action="{{ route('user.update', $user->id) }}" method="POST" id="new_form" class="border border-info p-3 rounded-3" enctype="multipart/form-data">
        @csrf
        {{ method_field('PUT') }}
        <h1 class="text-center">Edit Profile</h1>
        <div class="mb-3 row">
            <label for="name" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="name" id="name"
                       placeholder="Enter name" value="{{ $user->name }}">
                <small class="text-danger" id="checkname">* Fail</small>
            </div>
        </div>
        <div class="mb-3 row">
            <label for="email" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" name="email" id="email"
                       placeholder="Enter email" value="{{ $user->email }}">
                <small class="text-danger" id="checkemail">* Fail</small>
            </div>
        </div>
        <div class="mb-3 row">
            <label for="password" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" name="password" id="password"
                       placeholder="***********">
                <small class="text-danger" id="checkpassword">* Fail</small>
            </div>
        </div>
        <div class="mb-3 row">
            <label for="birthdate" class="col-sm-2 col-form-label">Birthdate</label>
            <div class="col-sm-10">
                <input type="date" class="form-control" id="birthdate" name="birthdate" value="{{ $user->birthdate }}">
                <small class="text-danger" id="checkbod">* Fail</small>
            </div>
        </div>
        <div class="mb-3 row">
            <div class="col-sm-12">
                <div class="p-0 form-check">
                    <input type="submit" class="btn btn-primary float-end" id="submit" value="Submit">
                    <a href="{{ route('user.index') }}" class="btn btn-danger">Cancel</a>
{{--                    <input type="cancel"  value="Cancel" name="cancel" id="cancel" class="btn btn-danger float-end">--}}
                </div>
            </div>
        </div>
    </form>
@endsection
