@extends('welcome')
@section('content')
    <div>
        <h1>Manage User</h1>
        <a href="{{ route('user.create') }}" class="btn btn-primary">Create User</a>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>BirthDate</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $key => $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->birthdate }}</td>
                <td>
                    <a href="{{ route('user.edit',$user->id) }}" class="">Edit</a>
                    <a href="{{ route('user.delete',$user->id) }}" class="text-danger" onclick="return confirm('Are you sure?')">Delete</a>
{{--                    <form id="delete-form-{{$user->id}}"--}}
{{--                          + action="{{route('users.destroy', $user->id)}}"--}}
{{--                          method="post">--}}
{{--                        @csrf @method('DELETE')--}}
{{--                    </form>--}}
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        <div class="number-pages">
            {{ $users->links() }}
        </div>
    </div>

@endsection
