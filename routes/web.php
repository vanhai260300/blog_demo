<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/user',\App\Http\Controllers\UserController::class)->middleware('is_admin');
Route::resource('/entry',\App\Http\Controllers\Website\EntryController::class)->middleware('is_admin');
Route::get('/entry-delete/{id}',[\App\Http\Controllers\Website\EntryController::class, 'destroy'])->name('entry.delete');
Route::get('/user-delete/{id}',[\App\Http\Controllers\UserController::class, 'destroy'])->name('user.delete');
//Route::get('/users', [\App\Http\Controllers\Website\UserController::class,'index'])->name('website.index');

Route::get('/', [\App\Http\Controllers\Website\HomeController::class, 'index'])->name('website.index');
Route::post('/comment',[\App\Http\Controllers\Website\HomeController::class, 'comment'])->name('website.comment');
Route::post('/changefollow',[\App\Http\Controllers\Website\HomeController::class, 'changeFollow'])->name('website.changefollow');
Route::get('/login', [\App\Http\Controllers\Website\Auth\LoginController::class, 'index'])->name('website.login.index');
Route::post('/login', [\App\Http\Controllers\Website\Auth\LoginController::class, 'login'])->name('website.login');
Route::get('/logout', [\App\Http\Controllers\Website\Auth\LoginController::class, 'logout'])->name('website.logout');
Route::get('/signup', [\App\Http\Controllers\Website\Auth\RegisterController::class, 'index'])->name('website.signup');
Route::post('/signup', [\App\Http\Controllers\Website\Auth\RegisterController::class, 'register'])->name('website.signup');
