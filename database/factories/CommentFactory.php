<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'comment'=> $this->faker->text($maxNbChars = 100),
            'user_id'=> rand(1,10),
            'entry_id' => rand(1,10),
        ];
    }
}
