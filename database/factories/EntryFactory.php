<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class EntryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'content' => $this->faker->text($maxNbChars = 650),
            'image' => $this->faker->imageUrl(140, 140, 'cats'),
            'post_date' => now(),
            'user_id' => rand(1, 10),
        ];
    }
}
